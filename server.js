require("dotenv").config();
const express = require("express");
const cors = require("cors");

const app = express();
const PORT = process.env.PORT || 3333;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const authRoutes = require("./src/routes/auth.route");
const userRoutes = require("./src/routes/user.route");

app.use("/api", authRoutes, userRoutes);

app.get("/", (req, res) => res.send("Welcome to PrivyId"));

app.all("*", (req, res) =>
  res.send("You've tried reaching a route that doesn't exist.")
);

app.listen(PORT, () => {
  console.log(`Server up on http://localhost:${PORT}`);
});

module.exports = app;
