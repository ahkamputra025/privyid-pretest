const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = require("chai").expect;
const app = require("../server");

chai.use(chaiHttp);
chai.should();

const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjMzODUxNjg3LCJleHAiOjE2MzY0NDM2ODd9.fTIlxPQqVAhGlzaggRCGmgQ7dEBBXZ2Cp_fFI68re4A";

describe("User", () => {
  describe("Add Balance", () => {
    it("Should POST Balance", (done) => {
      chai
        .request(app)
        .post("/api/addBalance")
        .set({ Authorization: token })
        .send({
          balance: 100000,
          ip: "192.168.25.2",
          location: "Jakarta",
          userAgent: "User Agent",
          author: "Author",
        })
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal("Add Balance Successfully.");
          expect(res.body).to.have.property("result");
          done();
        });
    });
  });

  describe("Transfer Balance", () => {
    it("Should POST Transfer Balance", (done) => {
      chai
        .request(app)
        .post("/api/transfer")
        .set({ Authorization: token })
        .send({
          transferAmount: 50000,
          emailDestination: "ahkamputra@gmail.com",
          ip: "192.168.25.2",
          location: "Jakarta",
          userAgent: "User Agent",
          author: "Author",
        })
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal("Transfer Balance Successfully.");
          done();
        });
    });
  });
});
