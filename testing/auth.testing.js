const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = require("chai").expect;
const app = require("../server");

chai.use(chaiHttp);
chai.should();

describe("Auth", () => {
  describe("Login", () => {
    it("Should GET login", (done) => {
      chai
        .request(app)
        .post("/api/login")
        .send({
          email: "jhondoe@gmail.com",
          password: "12345",
        })
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal("Login Successfully.");
          expect(res.body).to.have.property("token");
          done();
        });
    });
  });
});
