"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class USER_BALANCE_HISTORY extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  USER_BALANCE_HISTORY.init(
    {
      userBalanceId: DataTypes.INTEGER,
      balanceBefore: DataTypes.INTEGER,
      balanceAfter: DataTypes.INTEGER,
      activity: DataTypes.STRING,
      type: DataTypes.ENUM("debit", "kredit"),
      ip: DataTypes.STRING,
      location: DataTypes.STRING,
      userAgent: DataTypes.STRING,
      author: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "USER_BALANCE_HISTORIES",
      modelName: "USER_BALANCE_HISTORY",
    }
  );
  return USER_BALANCE_HISTORY;
};
