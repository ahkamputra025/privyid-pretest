"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class BANK_BALANCES extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BANK_BALANCES.init(
    {
      balance: DataTypes.INTEGER,
      balance_achieve: DataTypes.INTEGER,
      code: DataTypes.STRING,
      enable: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      tableName: "BANK_BALANCES",
      modelName: "BANK_BALANCES",
    }
  );
  return BANK_BALANCES;
};
