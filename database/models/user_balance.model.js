"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class USER_BALANCES extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  USER_BALANCES.init(
    {
      userId: DataTypes.INTEGER,
      balance: DataTypes.INTEGER,
      balanceAchieve: DataTypes.INTEGER,
    },
    {
      sequelize,
      tableName: "USER_BALANCES",
      modelName: "USER_BALANCES",
    }
  );
  return USER_BALANCES;
};
