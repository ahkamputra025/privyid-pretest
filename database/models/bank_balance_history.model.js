"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class BANK_BALANCE_HISTORY extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BANK_BALANCE_HISTORY.init(
    {
      bankBalanceId: DataTypes.INTEGER,
      balanceBefore: DataTypes.INTEGER,
      balanceAfter: DataTypes.INTEGER,
      activity: DataTypes.STRING,
      type: DataTypes.ENUM("debit", "kredit"),
      ip: DataTypes.STRING,
      location: DataTypes.STRING,
      userAgent: DataTypes.STRING,
      author: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "BANK_BALANCE_HISTORIES",
      modelName: "BANK_BALANCE_HISTORY",
    }
  );
  return BANK_BALANCE_HISTORY;
};
