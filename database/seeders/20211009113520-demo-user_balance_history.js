"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "USER_BALANCE_HISTORIES",
      [
        {
          userBalanceId: 1,
          balanceBefore: 100000,
          balanceAfter: 0,
          activity: "activiti1",
          type: "kredit",
          ip: "192.168.25.2",
          location: "Jakarta",
          userAgent: "userAgent1",
          author: "author1",
        },
        {
          userBalanceId: 2,
          balanceBefore: 200000,
          balanceAfter: 0,
          activity: "activiti2",
          type: "kredit",
          ip: "192.168.25.3",
          location: "Jogja",
          userAgent: "userAgent2",
          author: "author2",
        },
        {
          userBalanceId: 3,
          balanceBefore: 300000,
          balanceAfter: 0,
          activity: "activiti3",
          type: "kredit",
          ip: "192.168.25.4",
          location: "Malang",
          userAgent: "userAgent3",
          author: "author3",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
