"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "BANK_BALANCE_HISTORIES",
      [
        {
          bankBalanceId: 1,
          balanceBefore: 600000,
          balanceAfter: 0,
          activity: "activity1",
          type: "kredit",
          ip: "192.168.20.2",
          location: "Jakarta",
          userAgent: "userAgent1",
          author: "author1",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
