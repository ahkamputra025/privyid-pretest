"use strict";
const pwdHash = require("password-hash");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "USERS",
      [
        {
          username: "John Doe",
          email: "jhondoe@gmail.com",
          password: pwdHash.generate("12345"),
        },
        {
          username: "Jane Doe",
          email: "janedoe@gmail.com",
          password: pwdHash.generate("12345"),
        },
        {
          username: "Ahkam Putra",
          email: "ahkamputra@gmail.com",
          password: pwdHash.generate("12345"),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
