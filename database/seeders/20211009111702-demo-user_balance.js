"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "USER_BALANCES",
      [
        {
          userId: 1,
          balance: 100000,
          balanceAchieve: 5000,
        },
        {
          userId: 2,
          balance: 200000,
          balanceAchieve: 10000,
        },
        {
          userId: 3,
          balance: 300000,
          balanceAchieve: 15000,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
