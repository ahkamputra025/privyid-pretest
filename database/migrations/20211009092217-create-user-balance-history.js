"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("USER_BALANCE_HISTORIES", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      userBalanceId: {
        type: Sequelize.INTEGER,
        references: { model: "USER_BALANCES", key: "id" },
      },
      balanceBefore: {
        type: Sequelize.INTEGER,
      },
      balanceAfter: {
        type: Sequelize.INTEGER,
      },
      activity: {
        type: Sequelize.STRING,
      },
      type: {
        type: Sequelize.ENUM("debit", "kredit"),
      },
      ip: {
        type: Sequelize.STRING,
      },
      location: {
        type: Sequelize.STRING,
      },
      userAgent: {
        type: Sequelize.STRING,
      },
      author: {
        type: Sequelize.STRING,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("USER_BALANCE_HISTORIES");
  },
};
