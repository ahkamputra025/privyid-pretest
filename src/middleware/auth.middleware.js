const jwt = require("jsonwebtoken");
const method = {};

method.authToken = async (req, res, next) => {
  const token = req.header("Authorization");
  if (!token)
    return res.status(401).json({ message: "Failed to authenticate token." });

  try {
    const verifiedToken = await jwt.verify(token, process.env.SECRET_KEY);
    req.token = verifiedToken;
    next();
  } catch (error) {
    res.status(400).json({ message: "No token provided" });
  }
};

module.exports = method;
