const router = require("express").Router();
const {
  addBalance,
  transferBalance,
} = require("../controllers/user.controller");
const auth = require("../middleware/auth.middleware");

router.post("/addBalance", auth.authToken, addBalance);
router.post("/transfer", auth.authToken, transferBalance);

module.exports = router;
