const { USERS } = require("../../database/models");
const jwt = require("jsonwebtoken");
const pwdHash = require("password-hash");
const method = {};

method.login = async (req, res) => {
  try {
    const email = req.body.email.toLowerCase();

    const dtUser = await USERS.findOne({ where: { email: email } });

    if (dtUser) {
      const pwdVerify = await pwdHash.verify(
        req.body.password,
        dtUser.password
      );
      if (pwdVerify === true) {
        const token = jwt.sign({ id: dtUser.id }, process.env.SECRET_KEY, {
          expiresIn: "30d",
        });
        res.header("Authorization", token);
        res.status(200).json({
          statusCode: 200,
          message: "Login Successfully.",
          token: token,
        });
      } else {
        res.status(404).json({
          statusCode: 404,
          message: "Wrong Password.",
        });
      }
    } else {
      res.status(404).json({
        statusCode: 404,
        message: "Email Not Registered.",
      });
    }
  } catch (error) {
    res.status(400).json({
      statusCode: 400,
      message: error.message,
    });
  }
};

module.exports = method;
