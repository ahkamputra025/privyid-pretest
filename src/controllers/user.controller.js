const {
  USERS,
  USER_BALANCES,
  USER_BALANCE_HISTORY,
  BANK_BALANCES,
  BANK_BALANCE_HISTORY,
} = require("../../database/models");
const method = {};

method.addBalance = async (req, res) => {
  try {
    const userId = req.token.id,
      addBalance = req.body.balance;

    const findUserBalances = await USER_BALANCES.findOne({
      where: { id: userId },
    });

    if (findUserBalances) {
      //COUNT NEW BALANCE
      const newBalance = findUserBalances.balance + addBalance;
      //COUNT NEW BALANCE ACHIEVE
      const newBalanceAchieve =
        (addBalance * 5) / 100 + findUserBalances.balanceAchieve;

      //UPDATE USER_BALANCES, add New Balance
      await USER_BALANCES.update(
        { balance: newBalance, balanceAchieve: newBalanceAchieve },
        { where: { userId: userId } }
      );

      //UPDATE USER_BALANCE_HISTORY
      await USER_BALANCE_HISTORY.create({
        userBalanceId: userId,
        balanceBefore: findUserBalances.balance,
        balanceAfter: newBalance,
        activity: "Add balance",
        type: "kredit",
        ip: req.body.ip,
        location: req.body.location,
        userAgent: req.body.userAgent,
        author: req.body.author,
      });

      //UPDATE BANK_BALANCES
      const findBankBalance = await BANK_BALANCES.findAll();
      const idBankBalances = findBankBalance[0].dataValues.id;
      //COUNT NEW BALANCE
      const newBalanceBankBalance =
        findBankBalance[0].dataValues.balance + addBalance;
      //COUNT NEW BALANCE ACHIEVE
      const newBalanceBankBalanceAchieve =
        (addBalance * 5) / 100 + findBankBalance[0].dataValues.balance_achieve;

      await BANK_BALANCES.update(
        {
          balance: newBalanceBankBalance,
          balance_achieve: newBalanceBankBalanceAchieve,
        },
        { where: { id: idBankBalances } }
      );

      //UPDATE BANK_BALANCE_HISTORIES
      await BANK_BALANCE_HISTORY.create({
        bankBalanceId: idBankBalances,
        balanceBefore: findBankBalance[0].dataValues.balance,
        balanceAfter: newBalanceBankBalance,
        activity: "Add Balance",
        type: "kredit",
        ip: req.body.ip,
        location: req.body.location,
        userAgent: req.body.userAgent,
        author: req.body.author,
      });

      //RESPAYLOAD
      const dtResult = {
        userId: userId,
        balance: newBalance,
        balanceAchieve: newBalanceAchieve,
      };
      res.status(200).json({
        statusCode: 200,
        message: "Add Balance Successfully.",
        result: dtResult,
      });
    } else {
      res.status(404).json({
        statusCode: 404,
        message: "User Not Registered.",
      });
    }
  } catch (error) {
    res.status(400).json({
      statusCode: 400,
      message: error.message,
    });
  }
};

method.transferBalance = async (req, res) => {
  try {
    const userId = req.token.id,
      transferAmount = req.body.transferAmount;

    const findUserBalances = await USER_BALANCES.findOne({
      where: { id: userId },
    });

    if (findUserBalances) {
      //CEK BALANCE MINIMAL TRANSFER
      if (findUserBalances.balance > 500) {
        //CEK EMAIL
        const findUserBalancesEmail = await USERS.findOne({
          where: { email: req.body.emailDestination },
        });

        if (!findUserBalancesEmail) {
          res.status(404).json({
            statusCode: 404,
            message: "Email Not Registered.",
          });
        } else if (findUserBalancesEmail.id === findUserBalances.id) {
          res.status(404).json({
            statusCode: 404,
            message: "can't transfer, this email is yours.",
          });
        } else {
          //UPDATE TRANSFER USER_BALANCES DESTINATION
          const findUserDestinationTransfer = await USER_BALANCES.findOne({
            where: { userId: findUserBalancesEmail.id },
          });

          await USER_BALANCES.update(
            { balance: findUserDestinationTransfer.balance + transferAmount },
            { where: { userId: findUserBalancesEmail.id } }
          );

          //UPDATE DECREASE USER_BALANCE
          await USER_BALANCES.update(
            {
              balance: findUserBalances.balance - transferAmount,
              balanceAchieve:
                (transferAmount * 5) / 100 + findUserBalances.balanceAchieve,
            },
            { where: { userId: userId } }
          );

          //UPDATE USER_BALANCE_HISTORY DESTINATION
          await USER_BALANCE_HISTORY.create({
            userBalanceId: findUserDestinationTransfer.id,
            balanceBefore: findUserDestinationTransfer.balance,
            balanceAfter: findUserDestinationTransfer.balance + transferAmount,
            activity: "Add balance",
            type: "kredit",
            ip: req.body.ip,
            location: req.body.location,
            userAgent: req.body.userAgent,
            author: req.body.author,
          });

          //UPDATE DECREASE USER_BALANCE_HISTORY
          await USER_BALANCE_HISTORY.create({
            userBalanceId: findUserBalances.id,
            balanceBefore: findUserBalances.balance,
            balanceAfter: findUserBalances.balance - transferAmount,
            activity: "Transfer",
            type: "debit",
            ip: req.body.ip,
            location: req.body.location,
            userAgent: req.body.userAgent,
            author: req.body.author,
          });

          //UPDATE BANK_BALANCES
          const findBankBalance = await BANK_BALANCES.findAll();
          const idBankBalances = findBankBalance[0].dataValues.id;

          await BANK_BALANCES.update(
            {
              balance_achieve:
                (transferAmount * 5) / 100 +
                findBankBalance[0].dataValues.balance_achieve,
            },
            { where: { id: idBankBalances } }
          );

          res.status(200).json({
            statusCode: 200,
            message: "Transfer Balance Successfully.",
          });
        }
      } else {
        res.status(404).json({
          statusCode: 404,
          message: "your balance is not enough to make a transfer.",
        });
      }
    }
  } catch (error) {
    res.status(400).json({
      statusCode: 400,
      message: error.message,
    });
  }
};

module.exports = method;
