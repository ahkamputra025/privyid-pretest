# Documentation

PRIVYID - PRETEST - BACKEND ENGINEER

# Getting started

1. Move yourself to the backend folder: `cd app`
2. Copy the .env.Example file and create a .env file and add the connection DB and other and SIGNATURE (can be any word)
3. Install sequelize-cli how dev-dependency and execute this `sequelize db:create` next `sequelize db:migrate` and `sequelize db:seed:all`
4. Install node-modules `$ npm i` and run with command `$ npm start`

# API List

Web service API

| Routes | EndPoint        | Description                                         |
| ------ | --------------- | --------------------------------------------------- |
| POST   | /api/login      | login user and get token for authentication         |
| POST   | /api/addBalance | Add balance and stored in table user_balance,       |
|        |                 | user_balance_history and bank_balance,              |
|        |                 | bank_balance_history                                |
| POST   | /api/transfer   | Transfer to another user and stored in table        |
|        |                 | user_balance, user_balance_history and bank_balance |
|        |                 |                                                     |
